Compilation produces 
```
test.cu(35): warning: calling a __host__ function("Foo::Foo") from a __host__ __device__ function("main::[lambda() (instance 2)]::[lambda() (instance 2)]") is not allowed

test.cu(35): warning: calling a __host__ function("Foo::~Foo") from a __host__ __device__ function("main::[lambda() (instance 2)]::~[lambda() (instance 2)]") is not allowed
```
even though the constructor and destructor should never be called on GPU. The output of the example is
```
constructing on CPU
enquing kernel
copy constructing on CPU
copy constructing on CPU
destructing on CPU
destructing on CPU
kernel enqued
kernel on GPU

copy constructing on CPU
copy constructing on CPU
enquing kernel
copy constructing on CPU
destructing on CPU
kernel enqued
destructing on CPU
destructing on CPU
kernel on GPU
destructing on CPU
```
The warning is caused by the wrapped call to the kernel.

This code is connected to https://stackoverflow.com/questions/62810274/calling-a-host-function-from-a-host-device-function.
