#include "cuda.h"
#include <functional>

#define RUN_CUDA 1

struct FooData {
	float pi;
	int size;
};

struct Foo{
	__host__ __device__
	Foo( ){
#ifdef  __CUDA_ARCH__
		printf("Foo constructing on GPU, this should not happen!\n");
#else
		printf("Foo constructing on CPU\n");	    
#endif	    
	}

	__host__ __device__
	Foo( const Foo & that ){
#ifdef  __CUDA_ARCH__
		printf("Foo copy constructing on GPU, this should not happen!\n");
#else
		printf("Foo copy constructing on CPU, this should not happen!\n");
#endif	    
	}
    
	__host__ __device__
	~Foo(){
#ifdef  __CUDA_ARCH__
		printf("Foo destructing on GPU, this should not happen!\n");
#else
		printf("Foo destructing on CPU\n");
#endif	    
	}

	//The stuff to be done on the GPU, static and callable both from CPU and GPU
	__host__ __device__
	int operator()( FooData& data, bool device = false ) const {
#ifdef  __CUDA_ARCH__
		printf("kernel on GPU: pi=%g\n", data.pi );
#else
		printf("kernel on CPU: pi=%g\n", data.pi );
#endif      
		return 0;
	}
};

template<typename Callable, typename CallableData>
struct CUDACallable{
	Callable& foo; //This must be reference, otherwise we will make a copy -> multiple calls to destruct
	CUDACallable( Callable& _foo ) : foo{_foo} {}
	inline void __device__ operator()( int i, CallableData& data, bool /*device*/ ) const { //Device body, will receive a copy of the data (happens in the kernel call)
		foo( data, true );
	}
	inline void operator()( int i, CallableData& data ) const { //Host body, receives a reference to the data
		foo( data );
	}
};

template<typename Callable, typename CallableData>
__global__ 
void kernel( int size, Callable & deviceBody, CallableData data ){ //Not: Makes a copy of the data object, can't use reference
	int i = blockDim.x*blockIdx.x + threadIdx.x;
	if( i < size ){
		deviceBody( i, data, true );
	}
}

#ifdef __CUDACC__

//Wrapper decides runtime to use CUDA or not
template<typename Callable, typename CallableData>
void wrapper( int size, Callable & body, CallableData& data ){
	if( RUN_CUDA ){//GpuConfig::get().useCuda()
		printf("Using CUDA\n");
		const int nblocks = 1, block = 1;
		//const int nblocks = (size + block-1)/block;
		//LOG(debug) << "Launching CUDA kernel " << name << ": start.";
		kernel<<<nblocks, block>>>( size, body, data );
		//LOG(debug) << "Launching CUDA kernel " << name << ": enqueued.";
		auto err = cudaGetLastError();
		if( err != cudaSuccess ){
			printf("CUDA error: %s\n", cudaGetErrorString(err));
		}
	} else {
		//UTILITY_OMP(parallel for)
		for( int i = 0; i < size; i++ ){
			body( i, data );
		}
	}
}

#else // fall back to CPU-only kernels

template<typename Callable, typename CallableData>
void wrapper( int size, Callable & body, CallableData& data ){
	//UTILITY_OMP(parallel for)
	for( int i = 0; i < size; i++ ){
		body( i, data );
	}
}

#endif // __CUDACC__

int main(int argc, char** argv) {
	Foo foo;
	FooData data{ 3.14, 3 };

	CUDACallable<Foo, FooData> fooc(foo);
	printf("enquing kernel\n");
	wrapper( 1, fooc, data );
	printf("kernel enqued\n");
	cudaDeviceSynchronize( );

	return 0;
}

