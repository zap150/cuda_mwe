#include "cuda.h"

struct Foo{
    Foo( ){
#ifdef  __CUDA_ARCH__
      printf("constructing on GPU\n");	    
#else
      printf("constructing on CPU\n");	    
#endif	    
    }

    //__host__ __device__
    Foo( const Foo & that ){
#ifdef  __CUDA_ARCH__
      printf("copy constructing on GPU\n");	    
#else
      printf("copy constructing on CPU\n");	    
#endif	    
    }
    
    //__host__ __device__
    ~Foo( ){
#ifdef  __CUDA_ARCH__
      printf("destructing on GPU\n");	    
#else
      printf("destructing on CPU\n");	    
#endif	    
    }
   
   __host__ __device__
   int bar( ) const {
#ifdef  __CUDA_ARCH__
      printf("kernel on GPU\n");	    
#else
      printf("kernel on CPU\n");	    
#endif	    
     return 0;
   }
};

template<typename CopyBody>
__global__ 
void kernel( CopyBody cBody ){
  cBody( );
}

template <typename CopyBody>
void wrapper( CopyBody && cBody ){
  printf("enquing kernel\n");
  kernel<<<1,1>>>( cBody );
  printf("kernel enqued\n");
}

int main(int argc, char** argv) {

  Foo foo;

  printf("enquing kernel\n");
  kernel<<<1,1>>>( [=] __device__ ( ) { foo.bar( ); } );
  printf("kernel enqued\n");
  cudaDeviceSynchronize( );

  printf("\n");

  wrapper( [=] __device__ ( ) { foo.bar( ); } );
  cudaDeviceSynchronize( );
  
  return 0;
}

